CC := gcc
CFLAGS := \
	-Wextra -Wall -std=c99 -pedantic \
	-fPIC -ffunction-sections -fvisibility=hidden
LDFLAGS := \
	-Wl,--gc-sections -Wl,--print-gc-sections -Wl,--as-needed \
	-Wl,--exclude-libs,ALL -Wl,-z,defs


all: a.out

a.out: main.o libalpha.so
	$(CC) $(CFLAGS) $(LDFLAGS) main.o -L. -lalpha -o $@

libalpha.so: alpha.o libbeta.a libgamma.so
	ld \
		--print-gc-sections \
		--undefined=alpha_fn_used \
		--gc-sections --gc-keep-exported \
		--relocatable alpha.o libbeta.a -o reloc.o
	$(CC) $(CFLAGS) $(LDFLAGS) -shared reloc.o -L. -lgamma -o $@

libbeta.a: beta.o
	ar rcs $@ $<

libgamma.so: gamma.o
	$(CC) $(CFLAGS) $(LDFLAGS) -shared $< -o $@


%.o: %.cpp
	$(CC) $(CFLAGS) -c $< -o $@


.PHONY: all clean

clean:
	$(RM) alpha.o beta.o gamma.o main.o reloc.o
	$(RM) a.out libalpha.so libbeta.a libgamma.so
