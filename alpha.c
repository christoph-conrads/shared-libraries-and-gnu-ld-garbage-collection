#include "alpha.h"
#include "beta.h"


int alpha_fn_used(int i)
{
	return i + beta_fn_used(i);
}


int alpha_fn_unused(int i)
{
	return -i;
}
