# Avoiding Unused Shared Libraries after Garbage Collection in GNU ld

## Introduction

The GNU binutils linker `ld` is able to remove unused sections from ELF files
with the command line option `--gc-sections`. `ld` is also able to link against
shared libraries only when necessary with the flag `--as-needed`. The garbage
collection may remove all references to a certain shared library and in this
case, the library would not need to be linked against. As of binutils 2.24, GNU
ld is first computing all referenced shared libraries *before* performing
garbage collection. Thus, unused libraries may be added as dependency.

This demo code demonstrates how to work around the fact that GNU ld computes
the list of shared libraries before garbage collecting sections.

A related discussion on the binutils mailing list titled "Useless `DT_NEEDED`
tags after Garbage Collection" can be found at the following address:

http://sourceware.org/ml/binutils/2018-10/msg00226.html

The code works with binutils 2.30 but not with binutils 2.26.1 because the
linker option `--gc-keep-sections` is missing. It is not evident from
the change log when this option was introduced.


## Usage

The goal of the demonstration is to acquire a library `libalpha.so` containing
the symbols
* `alpha_fn_used` and
* `alpha_fn_unused`

The library should *not* be linked against `libgamma.so`

To run the demo, ensure GCC and GNU ld are installed. Moreover, ld must be
relatively recent; ld 2.30 is known to work, ld 2.26.1 is known not to work.
Execute the following commands in the shell to build `libalpha.so`:
```sh
make
```
Check for the presence of the symbols `alpha_fn_used` and `alpha_fn_unused` in
`libalpha.so`:
```sh
nm --extern-only --defined-only libalpha.so
```
The dynamic libraries required by `libalpha.so` are listed by `ldd`:
```sh
ldd libalpha.so
```
The library `libgamma.so` should *not* be shown.
